package com.tsystems.javaschool.tasks.calculator;

public class Divide extends Base implements Expression {
    public Divide(Expression left, Expression right) throws ErrorOperationException {
        super(left, right);
        if(left == null || right == null)
            throw new ErrorOperationException();
    }

    @Override
    public double perform(double x, double y) {
        if(y == 0){
            throw new IllegalArgumentException("Argument 'divisor' is 0");
        }
        return x / y;
    }
}
