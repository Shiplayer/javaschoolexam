package com.tsystems.javaschool.tasks.calculator;

public class Substraction extends Base {
    public Substraction(Expression exp){
        super(exp, new Const(0));
    }

    @Override
    public double perform(double x, double y) {
        return -x;
    }
}
