package com.tsystems.javaschool.tasks.calculator;

public class Add extends Base implements Expression {
    public Add(Expression left, Expression right) throws ErrorOperationException {
        super(left, right);
        if(left == null || right == null)
            throw new ErrorOperationException();
    }

    @Override
    public double perform(double x, double y) {
        return x + y;
    }
}
