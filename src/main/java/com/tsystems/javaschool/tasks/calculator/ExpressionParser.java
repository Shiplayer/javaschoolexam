package com.tsystems.javaschool.tasks.calculator;

public class ExpressionParser {

    private Lexer y;
    static private String line;

    private class Lexer {
        private final String s;
        private int count;

        Lexer(String s) {
            this.s = s;
            count = 0;
        }

        String next() {
            if (count == s.length()) {
                return "";
            }
            if (Character.isDigit(s.charAt(count)) || s.charAt(count) == '.') {
                int j = count;
                while (j + 1 < s.length() && (Character.isDigit(s.charAt(j + 1)) || s.charAt(j + 1) == '.') ) {
                    j++;
                }
                int buff = count;
                count = j + 1;
                return s.substring(buff, j + 1);
            }
            count++;
            return s.substring(count - 1, count);
        }

        String nextParenthesis(){
            int depth = 1;
            int start = count;
            while(count != s.length()){
                switch (s.charAt(count++)){
                    case '(':
                        depth++;
                        break;
                    case ')':
                        depth--;
                        if(depth == 0)
                            return s.substring(start, count - 1);

                }
            }
            return null;
        }
    }

    private Expression parseParenthesis() throws ErrorOperationException {
        return new ExpressionParser().parse(y.nextParenthesis());
    }

    private Expression parseValue() throws ErrorOperationException {
        if (line.charAt(0) >= '0' && line.charAt(0) <= '9') {
            return new Const(Double.parseDouble(line));
        } else if (line.equals("(")) {
            return parseParenthesis();
        }
        return null;
    }

    private Expression parseMultiplier() throws ErrorOperationException {
        String prev = line;
        line = y.next();
        if(line.equals(prev))
            throw new ErrorOperationException();
        else if (line.equals("-")) {
            return new Substraction(parseMultiplier());
        }
        return parseValue();
    }

    private Expression parseSum() throws ErrorOperationException {
        Expression left = parseMultiplier();
        if(left == null){
            return null;
        }
        while (true) {
            line = y.next();
            if (!line.equals("*") && !line.equals("/")) {
                return left;
            }
            if (line.equals("*")) {
                left = new Multiply(left, parseMultiplier());
            } else if (line.equals("/")) {
                left = new Divide(left, parseMultiplier());
            }
            if(left == null)
                return null;
        }
    }

    private Expression parseExpr() throws ErrorOperationException {
        Expression left = parseSum();
        if(left == null)
            return null;
        while (true) {
            if (line.equals("") || left == null) {
                return left;
            }
            switch (line.charAt(0)) {
                case '+': {
                    left = new Add(left, parseSum());
                    break;
                }
                case '-': {
                    left = new Subtract(left, parseSum());
                    break;
                }
                default:
                    return null;
            }
        }
    }

    public Expression parse(String statement) throws ErrorOperationException {
        y = new Lexer(statement);
        return parseExpr();
    }
}
