package com.tsystems.javaschool.tasks.calculator;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here
        if(statement == null || statement.isEmpty())
            return null;
        statement = statement.replaceAll("\\s", "");
        char prev = statement.charAt(0);
        for(int i = 1; i < statement.length(); i++){
            char ch = statement.charAt(i);

        }
        Expression ans;
        DecimalFormat df = new DecimalFormat("#.####");
        String str;
        df.setRoundingMode(RoundingMode.CEILING);
        try {
            ans = new ExpressionParser().parse(statement);
            str = df.format(ans.evaluate());
        } catch (NullPointerException | ErrorOperationException | IllegalArgumentException e){
            return null;
        }
        return str.replace(",", ".");
    }
}
