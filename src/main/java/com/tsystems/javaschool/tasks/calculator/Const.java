package com.tsystems.javaschool.tasks.calculator;

public class Const implements Expression{
    private double x;
    public Const(double x){
        this.x = x;
    }


    @Override
    public double evaluate() {
        return x;
    }
}
