package com.tsystems.javaschool.tasks.calculator;

public class Multiply extends Base implements Expression {
    public Multiply(Expression left, Expression right) throws ErrorOperationException {
        super(left, right);
        if(left == null || right == null)
            throw new ErrorOperationException();
    }

    @Override
    public double perform(double x, double y) {
        return x * y;
    }
}
