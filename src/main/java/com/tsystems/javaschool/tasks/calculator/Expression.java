package com.tsystems.javaschool.tasks.calculator;

public interface Expression {

    public double evaluate();

}
