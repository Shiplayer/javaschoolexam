package com.tsystems.javaschool.tasks.calculator;

public abstract class Base implements Expression {

	Expression ex1, ex2;

	Base(Expression ex1, Expression ex2) {

		this.ex1 = ex1;
		this.ex2 = ex2;
	}

	public double evaluate() {
		return perform(ex1.evaluate(), ex2.evaluate());
	}

	public abstract double perform(double x, double y);
}
