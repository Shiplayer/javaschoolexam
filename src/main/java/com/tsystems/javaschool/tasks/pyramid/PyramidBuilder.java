package com.tsystems.javaschool.tasks.pyramid;

import java.util.*;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        for(Integer buf: inputNumbers){
            if(buf == null)
                throw new CannotBuildPyramidException();
        }
        try {
            inputNumbers.sort(Integer::compareTo);
        } catch (OutOfMemoryError e){
            throw new CannotBuildPyramidException();
        }
        int row = 0, col = 0, totalSize = 0, next = 1;
        for (int buf : inputNumbers) {
            totalSize++;
            if(totalSize == next){
                row++;
                col++;
                if(col == 1)
                    next = next + 2;
                else {
                    col++;
                    next = next + col / 2 + 2;
                }
            }
        }
        if(inputNumbers.size() != next - col / 2 - 2) {
            throw new CannotBuildPyramidException();
        }
        int[][] matrix = new int[row][col];
        int middle = col / 2, count = 0;
        for(int i = 0; i < row; i++){
            if(i == 0){
                matrix[i][middle] = inputNumbers.get(count++);
            } else{
                int buf = (i + 1) * 2 - 1;
                int start = middle - buf / 2;
                for(int j = 0; j < buf; j++){
                    if(j % 2 == 0){
                        matrix[i][start + j] = inputNumbers.get(count++);
                    }
                }
            }
        }
        return matrix;
    }
}
