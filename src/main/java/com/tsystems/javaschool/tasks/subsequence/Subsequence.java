package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here
        int lengthX, lengthY;
        try {
            lengthX = x.size();
            lengthY = y.size();
        } catch (java.lang.Exception e) {
            throw new IllegalArgumentException();
        }
        if(lengthX == 0 && lengthY == 0){
            return true;
        }
        int count = 0;
        for(int i = 0; i < lengthY; i++){
            Object o = y.get(i);
            if(count == x.size()){
                return true;
            }
            if(o == x.get(count)){
                count++;
            }

        }
        return count == x.size();
    }
}
